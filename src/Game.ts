import "phaser";
import { Preloader } from './scenes/Preloader';
import { Main } from './scenes/Main';
    
const config: GameConfig = {
    type: Phaser.AUTO,
    backgroundColor: "#d4b274",
    width: 800,
    height: 450,
    parent: "canvas",
    // scale: {
    //     mode: Phaser.Scale.FIT,
    //     width: 600,
    //     height: 450,
    //     zoom: 2
    // },
    physics: {
      default: 'arcade', 
      arcade: {
        fps: 60,
        gravity: { y: 0 },
        debug:true
      }        
        
    },
    scene: [
      Preloader,
      Main
    ]
};

const game = new Phaser.Game(config);